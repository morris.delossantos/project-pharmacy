import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  items: Array<{user: string; fname: string; lname: string; mname: string; email: string}> = [];

  constructor(private router: Router) {
    this.items.push(
      {
        user: localStorage.getItem('user'),
        fname: localStorage.getItem('fname'),
        lname: localStorage.getItem('lname'),
        mname: localStorage.getItem('mname'),
        email: localStorage.getItem('email')
      }
    );
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['getstarted']);
  }
}
