import { Interpolation } from '@angular/compiler';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-createacc',
  templateUrl: './createacc.page.html',
  styleUrls: ['./createacc.page.scss'],
})
export class CreateaccPage implements OnInit {

  email:any;
  password:any;
  passwordConfirm:any;
  firstName:any;
  middle:any;
  lastName:any;


  constructor(
    public api_service:ApiService,
    public toast: ToastController,
    public route: Router
  ) { }
  
  ngOnInit() {
  }

  async createAccount() {

    if (this.email == null){
      
      // yung toast ay pop up message sa baba
      
      const toast = await this.toast.create({

        // pop up Message, palitan mo nalang

        message: 'Please Enter your Email',
        duration: 2000
      });
      toast.present();
    
    }else if(this.password == null){
      const toast = await this.toast.create({
        message: 'Please put password',
        duration: 2000
      });
      toast.present();
    } else if(this.password != this.passwordConfirm){
      const toast = await this.toast.create({
        message: 'Password Doesnt match',
        duration: 2000
      });
      toast.present();
    }else if(this.firstName == null){
      const toast = await this.toast.create({
        message: 'Input firstname',
        duration: 2000
      });
      toast.present();
    }else if (this.lastName == null){
      const toast = await this.toast.create({
        message: 'Input lastname',
        duration: 2000
      });
      toast.present();
    }else{

      // Interation, ipapasok lahat ng data sa baba sa vriale na 'data'

      let data ={
        email:this.email,
        password:this.password,
        firstName:this.firstName,
        middle:this.middle,
        lastName:this.lastName
      }
      this.api_service.createAccount(data).subscribe((res:any)=> {
        console.log("Success===",res);
  
        
      },(error)=> {
        console.log("Error===",error);
      })

      const toast = await this.toast.create({
        message: 'Success',
        duration: 2000
      });
      toast.present();


      // dadalhin ka sa login
      this.route.navigate(['/login']);

    }



    



    
    
  }
}
