/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/semi */
/* eslint-disable @typescript-eslint/quotes */
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  angForm: FormGroup;

  constructor(private fb: FormBuilder, private dataService: ApiService, private router: Router) {
    this.angForm = this.fb.group({
      email: ['', [Validators.required,Validators.minLength(1), Validators.email]],
      password: ['', Validators.required]
    });
  }

  message ={
    email: [
      {type:"required", message:"Please enter your Email"}
    ],
    password: [
      {type:"required", message:"Please enter your Password!"}
    ]
  }

  ngOnInit() {
  }
  postdata(angForm1)
  {
    this.dataService.userlogin(angForm1.value.email,angForm1.value.password)
    .pipe(first())
    .subscribe(
    data => {
      this.router.navigate(['home']);
    },
    error => {
      alert("Your Email or password is incorrect");
    });
  }
  get email() { return this.angForm.get('email'); }
  get password() { return this.angForm.get('password'); }
}
