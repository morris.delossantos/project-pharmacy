import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'getstarted',
    loadChildren: () => import('./getstarted/getstarted.module').then( m => m.GetstartedPageModule)
  },
  {
    path: '',
    redirectTo: 'getstarted',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'createacc',
    loadChildren: () => import('./createacc/createacc.module').then( m => m.CreateaccPageModule)
  },
  {
    path: 'antibts',
    loadChildren: () => import('./antibts/antibts.module').then( m => m.AntibtsPageModule)
  },
  {
    path: 'multi',
    loadChildren: () => import('./multi/multi.module').then( m => m.MultiPageModule)
  },
  {
    path: 'wound',
    loadChildren: () => import('./wound/wound.module').then( m => m.WoundPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
