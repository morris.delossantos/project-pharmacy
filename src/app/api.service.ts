/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/adjacent-overload-signatures */
/* eslint-disable @typescript-eslint/quotes */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { first, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  headers: HttpHeaders;

  constructor(
    public httpClient: HttpClient
  ) {
    this.headers = new HttpHeaders();
    this.headers.append("Accept", 'application/json');
    this.headers.append("Content-Type", 'application/json');
    this.headers.append("Access-Control-Allow-Origin", '*');
  }

  // palitan mo path, path lang to kung nasan ang php file

  createAccount(data){
    return this.httpClient.post('http://localhost/Projects/PharmacyFullstack/pharmacy/src/app/backend/create.php',data);
  }

  public userlogin(username, password) {
    return this.httpClient.post<any>('http://localhost/Projects/PharmacyFullstack/pharmacy/src/app/backend/login.php', { username, password })
    .pipe(map(Users => {
      this.setToken(Users[0].username, Users[0].first_name, Users[0].middle,
        Users[0].last_name, Users[0].email);
      return Users;
    }));
  }

  //token
  setToken(username: string, first_name: string, middle: string,
    lastname: string, email: string) {
    localStorage.setItem('user', username);
    localStorage.setItem('fname', first_name);
    localStorage.setItem('mname', middle);
    localStorage.setItem('lname', lastname);
    localStorage.setItem('email', email);
  }
  getToken() {
    return localStorage.getItem('token');
  }
  deleteToken() {
    localStorage.removeItem('token');
  }
}
